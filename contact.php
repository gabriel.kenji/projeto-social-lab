<?php
header("Content-type: application/json; charset=utf-8");
$name     = trim($_POST["name"]);
$subject  = "Contato via website";
$mailFrom = trim($_POST["email"]);
$message  = trim($_POST["message"]);
if ($name != null && $message != null && $mailFrom != null) {
    if (!filter_var($mailFrom, FILTER_VALIDATE_EMAIL) === false) {
        $mailTo  = "testesociallab@gmail.com"; /*Email que irá receber */
        $headers = "Enviado por: " . $mailFrom;
        $txt     = $name . "\n\n" . $message;
        mail($mailTo, $subject, $txt, $headers);
        $signal = "ok";
        $msg    = "Enviado";
    } else {
        $signal = "bad";
        $msg    = "Email inválido.";
    }
} else {
    $signal = "bad";
    $msg    = "Por favor preencha todos os campos.";
}
$data = array(
    'signal' => $signal,
    'msg' => $msg
);
echo json_encode($data);
?>