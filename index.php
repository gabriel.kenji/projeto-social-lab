<?php
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bootstrap Template Atlas</title>
    <meta name="description" content="Free bootstrap template Atlas">
    <link rel="icon" href="img/favicon.png" sizes="32x32" type="image/png">
    <!-- custom.css -->
    <link rel="stylesheet" href="css/custom.css">
    <!-- bootstrap.min.css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- font-awesome -->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
    <!-- AOS -->
    <link rel="stylesheet" href="css/aos.css">
</head>

<body>
    <!-- banner -->
    <div class="jumbotron jumbotron-fluid" id="banner" style="background-image: url(img/reagentes1.png);">
        <div class="container text-center text-md-left">
          <!--  <header>
                <div class="row justify-content-between">
                    <div class="col-2">
                        <img src="img/logo.png" alt="logo">
                    </div>
                    <div class="col-6 align-self-center text-right">
                        <a href="#" class="text-white lead">Get Early Access</a>
                    </div>
                </div>
            </header> -->
            <h1 data-aos="fade" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true" class="display-3 text-white font-weight-bold my-5">
            	Social<br>
            	<span style="display:inline-block; width: 15px;"></span> Lab
            </h1>
            <p data-aos="fade" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true" class="lead text-white my-4">
                Lorem ipsum dolor sit amet, id nec enim autem oblique, ei dico mentitum duo.
                <br> Illum iusto laoreet his te. Lorem partiendo mel ex. Ad vitae admodum voluptatum per.
            </p>
          <!--  <a href="#" data-aos="fade" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true" class="btn my-4 font-weight-bold atlas-cta cta-green">Get Started</a>-->
        </div>
    </div>

    <!-- feature (skew background) -->
    <div class="jumbotron jumbotron-fluid feature" id="feature-first">
        <div class="container my-5">
            <div class="row justify-content-between text-center text-md-left">
                <div data-aos="fade-right" data-aos-duration="1000" data-aos-once="true" class="col-md-6">
                    <h2 class="font-weight-bold">Um problema recorrente</h2>
                    <p class="my-4">Te iisque labitur eos, nec sale argumentum scribentur no,
                        <br> augue disputando in vim. Erat fugit sit at, ius lorem deserunt deterruisset no.</p>
                </div>
                <div data-aos="fade-left" data-aos-duration="1000" data-aos-once="true" class="col-md-6 align-self-center">
                    <img src="img/img1.png" alt="Take a look inside" class="mx-auto d-block">
                </div>
            </div>
        </div>
    </div>
    <!-- feature (green background) -->
    <div class="jumbotron jumbotron-fluid feature" id="feature-last">
        <div class="container">
            <div class="row justify-content-between text-center text-md-left">
                <div data-aos="fade-left" data-aos-duration="1000" data-aos-once="true" class="col-md-6 flex-md-last">
                    <h2 class="font-weight-bold">Nossa proposta</h2>
                    <p class="my-4">
                        Duo suas detracto maiestatis ad, commodo lucilius invenire nec ad,
                        <br> eum et oratio disputationi. Falli lobortis his ad
                    </p>
                </div>
                <div data-aos="fade-right" data-aos-duration="1000" data-aos-once="true" class="col-md-6 align-self-center flex-md-first">
                    <img src="img/img3.png" alt="Safe and reliable" class="mx-auto d-block imagem3">
                </div>
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>

    <!-- reagentes -->
    <div class="jumbotron jumbotron-fluid" id="reagentsection">
        <div class="container my-2">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <h2 class="font-weight-bold">Gostou da proposta?</h2>
                    <p class="my-4" style="color:white;">
                        ESPAÇO PARA TEXTO 
                    </p>
                </div>
                <div class="col-md-6">
                    <form id="reagentform" action="" method="post" style="width:550px;">
						<div class="form-group">
							<label for="name">Nome</label><br>
							<input type="text" name="name">
						</div>
						<div class="form-group" style="padding-left:4%;">
							<label for="email">Endereço de e-mail</label><br>
							<input type="text" name="email">
						</div><br>
						<div class="form-group">
							<input type="radio" name="radio" class="radio" value="busca de reagentes" checked />
							<label for="op1">Buscando</label>
							<input type="radio"  name="radio" class="radio" value="doação de reagentes"/>
							<label for="op2">Doando</label>
						</div>
						<div class="texts">
							<label for="message">Mensagem</label><br>
							<textarea class="form-control" name="message" rows="8"></textarea>
							<div id="msgr">
							</div><br>
							<center><button type="button" class="btn" id="submitreagentform" name="submit">Enviar</button></center>
						</div>
					</form>
                </div>
			</div>
        </div>
    </div>
	
	<!-- contato -->
    <div class="jumbotron jumbotron-fluid" id="contactsection">
        <div class="container my-2">
            <div class="row justify-content-between">
			<div class="col-md-2" id="form2">
                    <form id="contactform" action="" method="post" style="width:550px;">
						<div class="form-group">
							<label for="name">Nome</label><br>
							<input type="text" name="name">
						</div>
						<div class="form-group" style="padding-left:4%;">
							<label for="email">Endereço de e-mail</label><br>
							<input type="text" name="email">
						</div><br>
						<div class="texts">
							<label for="message">Mensagem</label><br>
							<textarea style="border: 1px solid #00FFAD; "class="form-control" name="message" rows="8"></textarea>
							<div id="msgc">
							</div><br>
							<center><button type="button" class="btn" id="submitcontactform" name="submit">Enviar</button></center>
						</div>
					</form>
                </div>
                <div class="col-md-7">
                    <h2 class="font-weight-bold">Fale Conosco</h2>
                    <p class="my-4">
                        Para dúvidas e sugestões, preencha os campos
                        <br>ao lado, ou se preferir, entre em contato diretamente:
                    </p>
                    <ul class="list-unstyled">
                        <li>Email : company_email@com</li>
                        <li>Telefone : 361-688-5824</li>
                        <li>Endereço : 4826 White Avenue, Corpus Christi, Texas</li>
                    </ul>
                </div>
			</div>
        </div>
    </div>
	
	
	
	<!-- copyright -->
	<div class="jumbotron jumbotron-fluid" id="copyright">
        <div class="container">
            <div class="row justify-content-between">
            	<div class="col-md-6 text-white align-self-center text-center text-md-left my-2">
                    Copyright © 2018.
                </div>
                <div class="col-md-6 align-self-center text-center text-md-right my-2" id="social-media">
                    <a href="#" class="d-inline-block text-center ml-2">
                    	<i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="d-inline-block text-center ml-2">
                    	<i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="d-inline-block text-center ml-2">
                    	<i class="fa fa-medium" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="d-inline-block text-center ml-2">
                    	<i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="js/aos.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	AOS.init({
	});
</script>
<script>
    $(document).ready(function(){
      $('#submitreagentform').click(function(event){
         $.ajax({
            dataType: 'json',
            url: 'reagent.php',
            type: 'POST',
            data: $('#reagentform').serialize(),
            beforeSend: function(xhr){
              $('#submitreagentform').html('Enviando...');
            },
            success: function(response){
              if(response){
                console.log(response);
                if(response['signal'] == 'ok'){
                 $('#msgr').html('<div class="alert alert-success col-md-10">'+ response['msg']  +'</div>');
                  $('input, textarea').val(function() {
                     return this.defaultValue;
                  });
                }
                else{
                  $('#msgr').html('<div class="alert alert-danger col-md-10">'+ response['msg'] +'</div>');
                }
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
				console.log(jqXHR.status);
				console.log(textStatus);
				console.log(errorThrown);
              $('#msgr').html('<div class="alert alert-danger col-md-10">Um erro ocorreu, tente novamente mais tarde.</div>');
            },
            complete: function(){
              $('#submitreagentform').html('Enviar');
            }
          });
      });
    });
</script>
<script>
    $(document).ready(function(){
      $('#submitcontactform').click(function(event){
         $.ajax({
            dataType: 'json',
            url: 'contact.php',
            type: 'POST',
            data: $('#contactform').serialize(),
            beforeSend: function(xhr){
              $('#submitcontactform').html('Enviando...');
            },
            success: function(response){
              if(response){
                console.log(response);
                if(response['signal'] == 'ok'){
                 $('#msgc').html('<div class="alert alert-success col-md-10">'+ response['msg']  +'</div>');
                  $('input, textarea').val(function() {
                     return this.defaultValue;
                  });
                }
                else{
                  $('#msgc').html('<div class="alert alert-danger col-md-10">'+ response['msg'] +'</div>');
                }
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
				console.log(jqXHR.status);
				console.log(textStatus);
				console.log(errorThrown);
              $('#msgc').html('<div class="alert alert-danger col-md-10">Um erro ocorreu, tente novamente mais tarde.</div>');
            },
            complete: function(){
              $('#submitcontactform').html('Enviar');
            }
          });
      });
    });
</script>
</html>
